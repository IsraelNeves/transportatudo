<?php
class Caminhao {
    private $id;
    private $placa;
    private $capacidade;


    public function getId() {
        return $this->id;
    }
    public function getPlaca() {
        return $this->placa;
    }
    public function getCapacidade() {
        return $this->capacidade;
    }
    public function getCaminhao($id) {
    require('banco.php');
        $stmt = $conn->prepare('SELECT placa, capacidade FROM caminhao WHERE idCaminhao=:id');
        $stmt->bindParam(":id", $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $respostaTexto = "Placa : ".$result['placa']." Capacidade : ".$result['capacidade'];
        return $respostaTexto;
    }
}